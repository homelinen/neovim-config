
call plug#begin('~/.nvim/bundle')

" Plug bundles {{{
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'zchee/deoplete-jedi'
Plug 'fishbullet/deoplete-ruby'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'airblade/vim-gitgutter'
Plug 'fatih/vim-go'
Plug 'godlygeek/tabular'
Plug 'ingydotnet/yaml-vim'
Plug 'lepture/vim-jinja'
Plug 'leshill/vim-json'
Plug 'mileszs/ack.vim'
Plug 'plasticboy/vim-markdown'
Plug 'scrooloose/nerdcommenter'
Plug 'w0rp/ale'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-ruby/vim-ruby'
Plug 'AlexMax/.vim'
Plug 'docker/docker', { 'rtp': '/contrib/syntax/vim/' }
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'justinj/vim-react-snippets'
Plug 'terryma/vim-expand-region'
Plug 'hashivim/vim-terraform'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" Themes
Plug 'romainl/Apprentice'
Plug 'chriskempson/vim-tomorrow-theme'

Plug 'git@github.com:voxpupuli/vim-puppet.git'

" Themes
Plug 'itchyny/lightline.vim'

call plug#end()
" }}}
" GENERAL CONFIG {{{
""""""""""""""""""""""""""""""""""""""""
"set encoding=utf8
nnoremap <space> <nop>
let mapleadermWhiteSpace="_"

if (system('uname') =~ "Darwin")
  let g:python_host_prog='/Users/calumgilchrist/.pyenv/versions/neovim2/bin/python'
  let g:python3_host_prog = '/Users/calumgilchrist/.pyenv/versions/neovim3/bin/python'
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  " gui colors if running iTerm
  if $TERM_PROGRAM =~ "iTerm"
    set termguicolors
  endif
  syntax on
  "set t_Co=256
  set background=dark
  colorscheme apprentice
  set hlsearch
  set incsearch
  set smartcase


  " Backspace through anything
  set backspace=indent,eol,start

  set scrolloff=5
  set display+=lastline

  set wildmenu

  set autoindent
  "set smartindent

  " Alternative escape
  imap ;; <Esc>

  " Mouse use in term
  set mouse=a

  " Relative line number stuff
  set relativenumber

  " Turn off when lost focus
  au FocusGained * :set relativenumber
  au FocusLost * :set number

  " Turn off in insert mode
  autocmd InsertEnter * :set number
  autocmd InsertLeave * :set relativenumber

  " End Line Numbers

  " Smart way to move between windows
  map <C-j> <C-W>j
  map <C-k> <C-W>k
  map <C-h> <C-W>h
  map <C-l> <C-W>l

  " Useful mappings for managing tabs
  map <leader>tN :tabnew<cr>
  map <leader>to :tabonly<cr>
  map <leader>tc :tabclose<cr>
  map <leader>tm :tabmove<CR>
  map <leader>tn :tabnext<CR>
  map <leader>tp :tabp<CR>

  " FZF
  map <C-p> :FZF<cr>

  " YCM
  "Mac sometimes gets upset
  "let g:ycm_path_to_python_interpreter = '/usr/bin/python'
  let g:ycm_collect_identifiers_from_tags_files = 1
  let g:ycm_seed_identifiers_with_syntax = 1
  let g:ycm_use_ultisnips_completer = 1

  " make YCM compatible with UltiSnips (using supertab)
  let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
  let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
  let g:SuperTabDefaultCompletionType = '<C-n>'

  " better key bindings for UltiSnipsExpandTrigger
  let g:UltiSnipsExpandTrigger = "<tab>"
  let g:UltiSnipsJumpForwardTrigger = "<tab>"
  let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
  " End YCM
  
  " Use JSX Highlighting on .js files
  let g:jsx_ext_required = 0

  "Add visible tabs
  set list
  set listchars=tab:\|\ 

  " Status Line
  function! CurDir()
      let curdir = substitute(getcwd(), '(/home/homelinen/|/Users/calumgilchrist)', "~/", "g")
      return curdir
  endfunction

  set showcmd "Show partial command in status line
  set statusline=%F%m%h\ %w\ \ [TYP=%Y]\ \ \ [POS=%l,%v][%p%%]\ \ %([%R%M]%)

  " Indentation Rules
  set expandtab
  set tabstop=2
  set shiftwidth=2

  if has('autocmd')
    filetype plugin indent on
  endif
endif

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif   

if has('python')
  let g:deoplete#enable_at_startup = 1
endif
