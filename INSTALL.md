# Installation

How to setup NeoVim with this repo

## Plug

Simple Plug-in manager: https://github.com/junegunn/vim-plug

### Download/Update

```sh
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
